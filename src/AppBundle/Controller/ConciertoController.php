<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Artista;
use AppBundle\Entity\Persona;
use AppBundle\Entity\Concierto;
use AppBundle\Entity\Pabellon;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ConciertoController extends Controller{
	/**
     * @Route("/createConcierto", name="createConcierto")
     */
    public function createConciertoAction(Request $request)
    {
        $concierto = new Concierto();

        $form = $this->createFormBuilder($concierto)
            ->add('name', TextType::class)
            ->add('precioentrada', NumberType::class)
            ->add('Artista',EntityType::class, array('class' => 'AppBundle:Artista','choice_label' => 'name'))
            ->add('Pabellon',EntityType::class, array('class' => 'AppBundle:Pabellon','choice_label' => 'name'))
            ->add('save', SubmitType::class, array('label' => 'Create Concierto'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($concierto);
            $em->flush();
            return new Response('Concierto inserted: '. $concierto->getId());
        }
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/selectConcierto", name="selectConcierto")
     */
     public function selectConciertoAction(Request $request)
    {
        $concierto = new Concierto();

        $form = $this->createFormBuilder($concierto)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select Concierto'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $conciertos=$em->getRepository('AppBundle:Concierto')->findByName($concierto->getName());
			return $this->render('default/conciertotaula.html.twig', array(
            'conciertos' => $conciertos
            ));
        }
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/selectConciertoall", name="selectConciertoall")
     */
     public function selectallConciertoAction()
    {
		 $em = $this->getDoctrine()->getManager();
         $conciertos=$em->getRepository('AppBundle:Concierto')->findAll();
         return $this->render('default/conciertotaula.html.twig', array(
            'conciertos' => $conciertos
            ));
	}
	/**
     * @Route("/actualitzaConcierto", name="actualitzaConcierto")
     */
     public function updateConciertoAction(Request $request)
    {
		$concierto = new Concierto();

        $form = $this->createFormBuilder($concierto)
            ->add('name', TextType::class)
            ->add('precioentrada', NumberType::class)
            ->add('Artista',EntityType::class, array('class' => 'AppBundle:Artista','choice_label' => 'name'))
            ->add('Pabellon',EntityType::class, array('class' => 'AppBundle:Pabellon','choice_label' => 'name'))
            ->add('save', SubmitType::class, array('label' => 'Update Concierto'))
            ->getForm();

        $form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$conciertos=$em->getRepository('AppBundle:Concierto')->findByName($concierto->getName());
			if(!$conciertos){
				throw $this->createNotFoundException(
				'No product found for name '); 
			}
			$conciertos[0]->setPrecioentrada($concierto->getPrecioentrada());
			$conciertos[0]->setArtista($concierto->getArtista());
			$conciertos[0]->setPabellon($concierto->getPabellon());
			$em->flush();
			return $this->render('default/conciertotaula.html.twig', array(
            'conciertos' => $conciertos
            ));
		}
		return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/deleteConcierto", name="deleteConcierto")
     */
     public function DeleteConciertoAction(Request $request)
    {
		 $concierto = new Concierto();

        $form = $this->createFormBuilder($concierto)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Borrar Concierto'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $conciertos=$em->getRepository('AppBundle:Concierto')->findOneByName($concierto->getName());
			if(!$conciertos){
				throw $this->createNotFoundException(
				'No se ha encontrado el Concierto '.$concierto->getName()); 
			}
			$em->remove($conciertos);
			$em->flush();
			return $this->render('default/deletetaula.html.twig');
		}
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
}
