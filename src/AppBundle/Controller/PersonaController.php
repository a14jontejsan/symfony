<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Artista;
use AppBundle\Entity\Persona;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class PersonaController extends Controller{
	/**
     * @Route("/createPersona", name="createPersona")
     */
    public function createPersonaAction(Request $request)
    {
        $persona = new Persona();

        $form = $this->createFormBuilder($persona)
            ->add('name', TextType::class)
            ->add('edat', NumberType::class)
            ->add('Artista',EntityType::class, array('class' => 'AppBundle:Artista','choice_label' => 'name'))
            ->add('save', SubmitType::class, array('label' => 'Create Persona'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($persona);
            $em->flush();
            return new Response('Persona inserted: '. $persona->getId());
        }
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/selectPersona", name="selectPersona")
     */
     public function selectPersonaAction(Request $request)
    {
        $persona = new Persona();

        $form = $this->createFormBuilder($persona)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select Persona'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $personas=$em->getRepository('AppBundle:Persona')->findByName($persona->getName());
			return $this->render('default/personataula.html.twig', array(
            'personas' => $personas
            ));
        }
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/selectPersonaall", name="selectPersonaall")
     */
     public function selectallPersonaAction()
    {
		 $em = $this->getDoctrine()->getManager();
         $personas=$em->getRepository('AppBundle:Persona')->findAll();
         return $this->render('default/personataula.html.twig', array(
            'personas' => $personas
            ));
	}
	/**
     * @Route("/actualitzaPersona", name="actualitzaPersona")
     */
     public function updatePersonaAction(Request $request)
    {
		 $persona = new Persona();

        $form = $this->createFormBuilder($persona)
            ->add('name', TextType::class)
            ->add('edat', NumberType::class)
            ->add('Artista',EntityType::class, array('class' => 'AppBundle:Artista','choice_label' => 'name'))
            ->add('save', SubmitType::class, array('label' => 'Update Persona'))
            ->getForm();

        $form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
            $personas=$em->getRepository('AppBundle:Persona')->findByName($persona->getName());
			if(!$personas){
				throw $this->createNotFoundException(
				'No product found for name '); 
			}
			$personas[0]->setEdat($persona->getEdat());
			$personas[0]->setArtista($persona->getArtista());
			$em->flush();
			return $this->render('default/personataula.html.twig', array(
            'personas' => $personas
            ));
		}
		return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/deletePersona", name="deletePersona")
     */
     public function DeletePersonaAction(Request $request)
    {
		 $persona = new Persona();

        $form = $this->createFormBuilder($persona)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Borrar Persona'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $personas=$em->getRepository('AppBundle:Persona')->findOneByName($persona->getName());
			if(!$personas){
				throw $this->createNotFoundException(
				'No se ha encontrado la Persona '.$persona->getName()); 
			}
			$em->remove($personas);
			$em->flush();
			return $this->render('default/deletetaula.html.twig');
		}
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
}
