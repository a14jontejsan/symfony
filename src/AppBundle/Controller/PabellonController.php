<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Artista;
use AppBundle\Entity\Persona;
use AppBundle\Entity\Concierto;
use AppBundle\Entity\Pabellon;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PabellonController extends Controller{
	/**
     * @Route("/createPabellon", name="createPabellon")
     */
    public function createConciertoAction(Request $request)
    {
        $pabellon = new Pabellon();

        $form = $this->createFormBuilder($pabellon)
            ->add('name', TextType::class)
            ->add('aforo', NumberType::class)
            ->add('localizacion', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Pabellon'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pabellon);
            $em->flush();
            return new Response('Pabellon inserted: '. $pabellon->getId());
        }
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/selectPabellon", name="selectPabellon")
     */
     public function selectPabellonAction(Request $request)
    {
        $pabellon = new Pabellon();

        $form = $this->createFormBuilder($pabellon)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select Pabellon'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $pabellones=$em->getRepository('AppBundle:Pabellon')->findByName($pabellon->getName());
			return $this->render('default/pabellontaula.html.twig', array(
            'pabellones' => $pabellones
            ));
        }
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/selectPabellonall", name="selectPabellonall")
     */
     public function selectallPabellonAction()
    {
		 $em = $this->getDoctrine()->getManager();
         $pabellones=$em->getRepository('AppBundle:Pabellon')->findAll();
         return $this->render('default/pabellontaula.html.twig', array(
            'pabellones' => $pabellones
            ));
	}
	/**
     * @Route("/actualitzaPabellon", name="actualitzaPabellon")
     */
     public function updatePabellonAction(Request $request)
    {
		$pabellon = new Pabellon();

        $form = $this->createFormBuilder($pabellon)
            ->add('name', TextType::class)
            ->add('aforo', NumberType::class)
            ->add('localizacion', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Update Pabellon'))
            ->getForm();

        $form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
            $pabellones=$em->getRepository('AppBundle:Pabellon')->findByName($pabellon->getName());
			if(!$pabellones){
				throw $this->createNotFoundException(
				'No product found for name '); 
			}
			$pabellones[0]->setAforo($pabellon->getAforo());
			$pabellones[0]->setLocalizacion($pabellon->getLocalizacion());
			$em->flush();
			return $this->render('default/pabellontaula.html.twig', array(
            'pabellones' => $pabellones
            ));
		}
		return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/deletePabellon", name="deletePabellon")
     */
     public function DeletePersonaAction(Request $request)
    {
		 $pabellon = new Pabellon();

        $form = $this->createFormBuilder($pabellon)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Borrar Pabellon'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $pabellones=$em->getRepository('AppBundle:Pabellon')->findOneByName($pabellon->getName());
			if(!$personas){
				throw $this->createNotFoundException(
				'No se ha encontrado el Pabellon '.$pabellon->getName()); 
			}
			$em->remove($pabellones);
			$em->flush();
			return $this->render('default/deletetaula.html.twig');
		}
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
}
