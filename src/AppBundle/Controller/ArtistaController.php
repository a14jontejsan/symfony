<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Artista;
use AppBundle\Entity\Persona;
use AppBundle\Entity\Concierto;
use AppBundle\Entity\Pabellon;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class ArtistaController extends Controller{
	/**
     * @Route("/createArtista", name="createArtista")
     */
    public function createArtistaAction(Request $request)
    {
        $artista = new Artista();

        $form = $this->createFormBuilder($artista)
            ->add('name', TextType::class)
            ->add('integrantes', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Crear Artista'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($artista);
            $em->flush();
            return new Response('Artista insertado: '. $artista->getId());
        }
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/selectArtista", name="selectArtista")
     */
     public function selectArtistaAction(Request $request)
    {
        $artista = new Artista();

        $form = $this->createFormBuilder($artista)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Seleccionar Artista'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $artistas=$em->getRepository('AppBundle:Artista')->findByName($artista->getName());
			return $this->render('default/artistataula.html.twig', array(
            'artistas' => $artistas
            ));
        }
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/selectArtistaall", name="selectArtistaall")
     */
     public function selectallArtistaAction()
    {
		 $em = $this->getDoctrine()->getManager();
         $artistas=$em->getRepository('AppBundle:Artista')->findAll();
         return $this->render('default/artistataula.html.twig', array(
            'artistas' => $artistas
            ));
	}
	/**
     * @Route("/actualitzaArtista", name="actualitzaArtista")
     */
     public function UpdateArtistaAction(Request $request)
    {
		$artista = new Artista();

        $form = $this->createFormBuilder($artista)
            ->add('name', TextType::class)
            ->add('integrantes', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Actualizar Artista'))
            ->getForm();

        $form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$artistas=$em->getRepository('AppBundle:Artista')->findByName($artista->getName());
			if(!$artistas){
				throw $this->createNotFoundException(
				'No se a encontrado el Artista '.$artista->getName()); 
			}
			$artistas[0]->setIntegrantes($artista->getIntegrantes());
			$em->flush();
			return $this->render('default/artistataula.html.twig', array(
            'artistas' => $artistas
            ));
		}
		return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
	/**
     * @Route("/deleteArtista", name="deleteArtista")
     */
     public function DeleteArtistaAction(Request $request)
    {
		 $artista = new Artista();

        $form = $this->createFormBuilder($artista)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Borrar Artista'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $artistas=$em->getRepository('AppBundle:Artista')->findOneByName($artista->getName());
			if(!$artistas){
				throw $this->createNotFoundException(
				'No se a encontrado el Artista '.$artista->getName()); 
			}
			$em->remove($artistas);
			$em->flush();
			return $this->render('default/deletetaula.html.twig');
		}
        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
	}
}
