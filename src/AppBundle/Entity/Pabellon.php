<?php
/// src/AppBundle/Entity/Pabellon.php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="pabellon")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PabellonRepository")
 */
 
class Pabellon
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $aforo;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $localizacion;
    
    /**
     * @ORM\OneToMany(targetEntity="Concierto", mappedBy="pabellon")
     */
    protected $concierto;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->concierto = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Pabellon
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set aforo
     *
     * @param integer $aforo
     *
     * @return Pabellon
     */
    public function setAforo($aforo)
    {
        $this->aforo = $aforo;

        return $this;
    }

    /**
     * Get aforo
     *
     * @return integer
     */
    public function getAforo()
    {
        return $this->aforo;
    }

    /**
     * Set localizacion
     *
     * @param string $localizacion
     *
     * @return Pabellon
     */
    public function setLocalizacion($localizacion)
    {
        $this->localizacion = $localizacion;

        return $this;
    }

    /**
     * Get localizacion
     *
     * @return string
     */
    public function getLocalizacion()
    {
        return $this->localizacion;
    }

    /**
     * Add concierto
     *
     * @param \AppBundle\Entity\Concierto $concierto
     *
     * @return Pabellon
     */
    public function addConcierto(\AppBundle\Entity\Concierto $concierto)
    {
        $this->concierto[] = $concierto;

        return $this;
    }

    /**
     * Remove concierto
     *
     * @param \AppBundle\Entity\Concierto $concierto
     */
    public function removeConcierto(\AppBundle\Entity\Concierto $concierto)
    {
        $this->concierto->removeElement($concierto);
    }

    /**
     * Get concierto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConcierto()
    {
        return $this->concierto;
    }
}
