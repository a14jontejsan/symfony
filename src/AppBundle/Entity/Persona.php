<?php
/// src/AppBundle/Entity/Persona.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="persona")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PersonaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
 
class Persona
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $edat;
    /**
     * @ORM\ManyToOne(targetEntity="Artista", inversedBy="persona", cascade={"all"})
     * @ORM\JoinColumn(name="artista_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $artista;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Persona
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set edat
     *
     * @param integer $edat
     *
     * @return Persona
     */
    public function setEdat($edat)
    {
        $this->edat = $edat;

        return $this;
    }

    /**
     * Get edat
     *
     * @return integer
     */
    public function getEdat()
    {
        return $this->edat;
    }

    /**
     * Set artista
     *
     * @param \AppBundle\Entity\Artista $artista
     *
     * @return Persona
     */
    public function setArtista(\AppBundle\Entity\Artista $artista = null)
    {
        $this->artista = $artista;

        return $this;
    }

    /**
     * Get artista
     *
     * @return \AppBundle\Entity\Artista
     */
    public function getArtista()
    {
        return $this->artista;
    }
}
