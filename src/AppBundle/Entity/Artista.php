<?php
/// src/AppBundle/Entity/Artista.php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="artista")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ArtistaRepository")
 */
 
class Artista
{
	/**
	 * 
	 * @var int
	 * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $integrantes;
    
    /**
	* @ORM\OneToMany(targetEntity="Persona", mappedBy="artista")
    */
    protected $persona;
    
    /**
     * @ORM\OneToMany(targetEntity="Concierto", mappedBy="artista")
     */
    protected $concierto;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->personas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->concierto = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Artista
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set integrantes
     *
     * @param integer $integrantes
     *
     * @return Artista
     */
    public function setIntegrantes($integrantes)
    {
        $this->integrantes = $integrantes;

        return $this;
    }

    /**
     * Get integrantes
     *
     * @return integer
     */
    public function getIntegrantes()
    {
        return $this->integrantes;
    }

    /**
     * Add persona
     *
     * @param \AppBundle\Entity\Persona $persona
     *
     * @return Artista
     */
    public function addPersona(\AppBundle\Entity\Persona $persona)
    {
        $this->personas[] = $persona;

        return $this;
    }

    /**
     * Remove persona
     *
     * @param \AppBundle\Entity\Persona $persona
     */
    public function removePersona(\AppBundle\Entity\Persona $persona)
    {
        $this->personas->removeElement($persona);
    }

    /**
     * Get personas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonas()
    {
        return $this->personas;
    }

    /**
     * Add concierto
     *
     * @param \AppBundle\Entity\Concierto $concierto
     *
     * @return Artista
     */
    public function addConcierto(\AppBundle\Entity\Concierto $concierto)
    {
        $this->concierto[] = $concierto;

        return $this;
    }

    /**
     * Remove concierto
     *
     * @param \AppBundle\Entity\Concierto $concierto
     */
    public function removeConcierto(\AppBundle\Entity\Concierto $concierto)
    {
        $this->concierto->removeElement($concierto);
    }

    /**
     * Get concierto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConcierto()
    {
        return $this->concierto;
    }

    /**
     * Get persona
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersona()
    {
        return $this->persona;
    }
}
