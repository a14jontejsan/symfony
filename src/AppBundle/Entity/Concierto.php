<?php
/// src/AppBundle/Entity/Concierto.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="concierto")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ConciertoRepository")
 */
 
class Concierto
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $precioentrada;
    
    /**
     * @ORM\ManyToOne(targetEntity="Artista", inversedBy="concierto", cascade={"all"})
     * @ORM\JoinColumn(name="artista_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $artista;
    
    /**
     * @ORM\ManyToOne(targetEntity="Pabellon", inversedBy="concierto", cascade={"all"})
     * @ORM\JoinColumn(name="pabellon_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $pabellon;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Concierto
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set precioentrada
     *
     * @param string $precioentrada
     *
     * @return Concierto
     */
    public function setPrecioentrada($precioentrada)
    {
        $this->precioentrada = $precioentrada;

        return $this;
    }

    /**
     * Get precioentrada
     *
     * @return string
     */
    public function getPrecioentrada()
    {
        return $this->precioentrada;
    }

    /**
     * Set artista
     *
     * @param \AppBundle\Entity\Artista $artista
     *
     * @return Concierto
     */
    public function setArtista(\AppBundle\Entity\Artista $artista = null)
    {
        $this->artista = $artista;

        return $this;
    }

    /**
     * Get artista
     *
     * @return \AppBundle\Entity\Artista
     */
    public function getArtista()
    {
        return $this->artista;
    }

    /**
     * Set pabellon
     *
     * @param \AppBundle\Entity\Pabellon $pabellon
     *
     * @return Concierto
     */
    public function setPabellon(\AppBundle\Entity\Pabellon $pabellon = null)
    {
        $this->pabellon = $pabellon;

        return $this;
    }

    /**
     * Get pabellon
     *
     * @return \AppBundle\Entity\Pabellon
     */
    public function getPabellon()
    {
        return $this->pabellon;
    }
}
